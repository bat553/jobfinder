let request = require("request");
let cheerio = require('cheerio');
let crypto = require('crypto');
let aws = require('aws-sdk');
//require('dotenv').config()

let bucketName = process.env.BucketName

aws.config.update({
/*    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,*/
    region: "eu-north-1"
});
let s3 = new aws.S3();

let Authentification = async function(){
    let options= {
        url : "https://www.itii-lyon.fr/identification-candidat.html",
        method: "POST",
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0'
        },
        form: {
            cand_login: process.env.USER,
            cand_passwd: process.env.PASS,
            formid: "form_login"
        }
    }
    return new Promise((resolve, reject) => {
        request(options, function (err, resp, body) {
            if (err){
                reject(err)
            } else if (resp['statusCode'] !== 302){
                reject(body)
            } else {
                resolve(resp['headers']['set-cookie'])
            }
        })
    })
}

let GetPostes = function(cookies){
    let options = {
        url: "https://www.itii-lyon.fr/postes-entreprises.html?Filiere=IRC",
        method: "GET",
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0',
            "Cookie": cookies
        }
    }
    return new Promise((resolve, reject) => {
        request(options, function (err, resp, body) {
            if (err){
                reject(err)
            } else if (resp['statusCode'] !== 200){
                reject(body)
            } else {
                resolve(body)
            }
        })
    })
}

let ExtractData = function(body){
    return new Promise((resolve, reject) =>{
        const $ = cheerio.load(body);
        const result = $("#offre_tab").text();
        const hash = crypto.createHash('sha256');
        resolve(hash.update(result).digest('hex'))
    })
}

let CompareHash = function(hash, new_hash){
    return new Promise((resolve, reject) => {
        if (hash === new_hash){
            resolve(true)
        } else {
            reject(new Error("Hash changed. Nouvelles annonces disponibles"))
        }
    })
}

let GetHash = function(){
    const params = {
        Bucket: bucketName,
        Key: "hash.txt"
    }
    return new Promise((resolve, reject) => {
        s3.getObject(params, function (err, data) {
            if (err){
                reject(err)
            } else {
                resolve(data['Body'].toString())
            }
        })
    })
}

let UpdateHash = function(new_hash){
    const params = {
        Bucket: bucketName,
        Body: Buffer.from(new_hash),
        Key: "hash.txt"
    }
    return new Promise((resolve, reject) => {
        s3.putObject(params, function (err, data) {
            if (err){
                reject(err)
            } else {
                resolve(true)
            }
        })
    })
}

exports.handler = async function main() {
    try {
        const cookies = await Authentification();
        const body = await GetPostes(cookies);
        const new_hash = await ExtractData(body);
        const hash = await GetHash();
        await UpdateHash(new_hash);
        await CompareHash(hash, new_hash);
        return "Pas de changements"
    } catch (e) {
        throw new Error(e)
    }
}
